var express = require('express');
var router = express.Router();
var bodyParser = require("body-parser");

router.use(bodyParser.json());

var cors = require('cors');

router.use(cors());

var callingtocall = require('../src/controller/User')

router.get('/', async function (req, res, next) {
    var allUser;
    await callingtocall.getAllUser(function (err, res) {
        if (err) {
            console.log(err)
        } else {
            allUser = res;
        }
    });
    await res.send(allUser);
});

router.post('/createUser', async function (req, res, next) {

    var createUser;
    console.log(req.body)
    await callingtocall.createUser(req.body, function (err, res) {
        if (err) {
            createUser = false;
        } else {
            console.log(res)
            if (res == null)
                createUser = false;
            else
                createUser = true;
        }
    })
    await res.send(createUser)
});

router.post('/updateUser', async function (req, res, next) {
    console.log(req.body)
    var updateUser = true;

    await callingtocall.checkUser(req.body.email, function (err, res) {
        if (err) {
            updateUser = false;
        } else {
            console.log(res)
            if (res == null) {
                callingtocall.updateUser({ email: req.body.email, status: req.body.status }, req.body.oldemail, function (err, res) {
                    if (err) {
                        updateUser = false;
                    } else {
                        if (res == null) {
                            updateUser = false;
                        }
                        else {
                            updateUser = true;
                        }
                    }
                })
            }
            else {
                updateUser = false;
            }
        }
    })

    await res.send(updateUser)
});

router.post('/deleteUser', async function (req, res, next) {
    var deleteUser;

    await callingtocall.deleteUser(req.body.email, function (err, res) {
        if (err) {
            deleteUser = false;
        } else {
            if (res == null)
                deleteUser = false;
            else
                deleteUser = true;
        }
    })
    await res.send(deleteUser)
});

router.post('/authUser', async function (req, res, next) {

    var authUser;

    await callingtocall.checkUser(req.body.email, function (err, res) {
        if (err) {
            authUser = false;
        } else {
            if (res == null)
                authUser = false;
            else
                authUser = res;
        }
    })
    await res.send(authUser)
});

module.exports = router;
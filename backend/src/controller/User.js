module.exports.createUser = async (data,cb) => {
    const User = require('../models/user');

    const errorHandler = (err,cb) => {
        console.error("Error: ", err);
        cb(err);
    }
    
    const userCreate = await User.create({ 
        email: data.email, 
        firstname: data.firstname, 
        lastname: data.lastname, 
        role: data.role, 
        status: data.status 
    }).then( function(data){
        return cb(null,data)
    }).catch(errorHandler);

    
};

module.exports.checkUser = async function (dataCheck,cb) {

    const User1 = require('../models/user');

    const errorHandler = err => {
        console.error("Error: ", err);
        return cb(err);
    }
    const userFind = await User1.findOne({ where: { email:dataCheck } }).then(function (data) {        
        return cb(null,data);
    }).catch(errorHandler);
}

module.exports.getAllUser = async function (cb) {

    const User1 = require('../models/user');

    const errorHandler = err => {
        console.error("Error: ", err);
        return cb(err);
    }

    const userFind = await User1.findAll().then(function (users) {        
        return cb(null,users);
    }).catch(errorHandler);
}

module.exports.updateUser = async function (data,emailID,cb) {

    const User1 = require('../models/user');

    const errorHandler = err => {
        console.error("Error: ", err)
    }

    const userUpdate = await User1.update(data, { where: { email: emailID } }).then(function(data){
        return cb(null,data);
    }).catch(errorHandler);
}

module.exports.deleteUser = async function (dataCheck,cb) {

    const User1 = require('../models/user');

    const errorHandler = err => {
        console.error("Error: ", err)
    }

    const userDelete = await User1.destroy({ where: { email:dataCheck } }).then(function (data) {        
        return cb(null,data);
    }).catch(errorHandler);
}


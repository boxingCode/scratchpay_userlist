const Sequelize = require("sequelize");

module.exports = sequelize.define("user", {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1,
        //autoIncrement : true,
        primaryKey:true
    },
    email: {
        type: Sequelize.STRING(100),
        unique: true,
        allowNull : false
    },
    firstname: {
        type: Sequelize.STRING(100)
    },
    lastname: {
        type: Sequelize.STRING(100)
    },
    role: {
        type: Sequelize.STRING(100)
    },
    status: {
        type: Sequelize.ENUM('active', 'inactive')
    }
})
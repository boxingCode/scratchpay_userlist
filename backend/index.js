const express = require('express');
const path = require('path');

const app = express();

var userRouter = require('./routes/user.route');

app.set('views', path.join(__dirname,'views'));
app.set('view engine', 'hbs');

//DB Connection
require("./src/database/connection");

//only called from routes userRouter/user.js using callingtocall.innedacall in controller/main.js
//require("./src/controller/main")();

app.use('/',userRouter);

const port = process.env.PORT || 3000;
app.listen(port, ()=> console.log("listening to port 3000") )
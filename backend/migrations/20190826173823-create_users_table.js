'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("users", {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1,
        //autoIncrement: true,
        primaryKey: true
      },
      email: {
        type: Sequelize.STRING(100),
        unique: true,
      },
      firstname: {
        type: Sequelize.STRING(100)
      },
      lastname: {
        type: Sequelize.STRING(100)
      },
      role: {
        type: Sequelize.STRING(100)
      },
      status: {
        type: Sequelize.ENUM('active', 'inactive')
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("users");
  }
};

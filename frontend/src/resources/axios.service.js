const axios = require('axios');

/**
 * Axios client Library for get request, url is set to localhost, but change it to get the request-serve
 * @param {String} path relative path to serve request
 * @param {Callback} Callback function to serve error
 * @param {Callback} Callback function to serve received data
 */
/**
 * @example
 * getService("authUser",{ email: this.useremail },
 * error => {console.log("ERROR" + error);},
 * Response => {
 * if (Response.data != null) {
 * this.$router.push("/user");}
 * }) 
 */
module.exports.getService = function (path, error, success) {
    axios({
        method: 'get',
        url: 'http://localhost:3000/'+path,
        config: { headers: {'Content-Type': 'multipart/form-data' }},
        crossDomain: true
    }).then(
        (response) => {
            success(response.data)
        },
        (err) => {
            error(err)
        }
    );
}

/**
 * Axios client Library for post request, url is set to localhost, but change it to get the request-serve
 * @param {String} path relative path to serve request
 * @param {String} Array data to send in request body
 * @param {Callback} Callback function to serve error
 * @param {Callback} Callback function to serve received data
 */

module.exports.postService = function (path,data, error, success) {
    axios({
        method: 'post',
        url: 'http://localhost:3000/'+path,
        data: data,
        config: { headers: {'Content-Type': 'multipart/form-data' }},
        crossDomain: true
    }).then(
        (response) => {
            success(response)
        },
        (err) => {
            error(err)
        }
    );
}
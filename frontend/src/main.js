import Vue from 'vue/dist/vue.js';
import App from './App.vue';


import customTable from './componentReuse/Customtable.vue'
import customnavigation from './componentReuse/Customnavigation.vue'


import BootstrapVue from 'bootstrap-vue';
Vue.use(BootstrapVue);
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
/**SWEET-ALERT */
import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);


/**ROURT START? */
import Router from 'vue-router';

import LoginRoute from './components/Login.vue'
import UserRoute from './components/User.vue'

Vue.use(Router);

const routes = [
    {
        path: '/',
        name: 'loginRoute',
        component: LoginRoute
    },
    {
        path: '/user',
        name: 'UserRoute',
        component: UserRoute
    }
    /*
        path: '/',
            name: 'Hello',
            component: layouts,
            children: [{
                path: '/todo/:id',
                name: 'todo',
                component: todo
            }]
        }
    */
];

const router = new Router({
    routes,
    mode: 'history'
})

/**ROUTER ENDS */

new Vue({
    router,
    render: h => h(App),
    components: {
        customTable,
        customnavigation
    }
}).$mount('#app');
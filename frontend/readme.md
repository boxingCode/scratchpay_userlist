***QUESTION***
The interface should be easy to use and all the elements should be obvious and self-explanatory as much as possible. There are some basic branding guidelines further down that should be used, but you're free to design a brand new interface that is unlike any others currently available at Scratchpay, and to be creative if something is not specified in the included guidelines.


***SOLUTION***

As the project wanted to have a client with a user table and some privileges to perform CURD operations, UI contain a user validation, where user enters its credential and getts logged into user dashboard page, where he sees a table with action to perform like update and delete, while logging in he can create his account and log-in using that.

Axios has been used to serve requests.
JSDoc to provide some documentation with examples.
Webpack is getting used to bundle the application.
Bootstrap framework is used to have a grid layout.

***USING APPLICATION***
git clone
do "npm install"
to run in development mode, do: "npm start"

!!NICE application is up and running
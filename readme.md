## Scratchpay Userlist

- Git clone of repo to have both backend and frontend apps.
- With each folder do "npm install" to get all dependencies.
- To serve request, change the port and url in frontend/src/resources/axios.service.js
- Done with the app running process, do npm start individually for both folder to view and serve request.

## Technologies
javascript

**frontend*
Framework : `vue.js`,
Http-request : `axios`,
`css and bootstrap`,
Package manager : `webpack`
es convertor : `babel`

1) jsdoc
2) sweetalert
3) vue-route

## backend
Framework : `express`,
Database : `postgresql`
1) cors
2) bodyparse
3) sequelizer
